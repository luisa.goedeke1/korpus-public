## Release Versions

Overview of the texts, number of annotated sentences, and annotated phenomena in each version.

### v5.1 (2023-07-27)

| Text                                                                                     |  Sentences | GI, Comment, NfR | Attr, Change |
| :--------------------------------------------------------------------------------------- | ---------: | :--------------: | :----------: |
| Andreae__Die_chymische_Hochzeit                                                          |      462   |                ✓ |            ✓ |
| Bürger__Münchhausen                                                                      |      620   |                ✓ |            ✓ |
| Dahn__Kampf_um_Rom_ab_Kapitel_2                                                          |      420   |                ✓ |            ✓ |
| Fontane__Der_Stechlin                                                                    |      580   |                ✓ |            ✓ |
| Gellert__Das_Leben_der_schwedischen_Gräfin_von_G                                         |      420   |                ✓ |            ✓ |
| Goethe__Die_Wahlverwandtschaften                                                         |      687   |                ✓ |            ✓ |
| Grillparzer__Der_arme_Spielmann                                                          |      420   |                ✓ |            ✓ |
| Grimmelshausen__Der_abenteuerliche_Simplicissimus                                        |      499   |                ✓ |            ✓ |
| Hoffmann__Der_Sandmann                                                                   |      411   |                ✓ |            ✓ |
| Hölderlin__Hyperion_oder_der_Eremit_in_Griechenland                                      |      440   |                ✓ |            ✓ |
| Kafka__Der_Bau                                                                           |      420   |                ✓ |            ✓ |
| Keller__Der_grüne_Heinrich                                                               |      490   |                ✓ |            ✓ |
| Kleist__Michael_Kohlhaas                                                                 |      443   |                ✓ |            ✓ |
| Knigge__Die_Reise_nach_Braunschweig                                                      |      460   |                ✓ |            ✓ |
| Kotzebue__Das_merkwürdigste_Jahr_meines_Lebens                                           |      470   |                ✓ |            ✓ |
| LaRoche__Geschichte_des_Fräuleins_von_Sternheim                                          |      420   |                ✓ |            ✓ |
| Lewald__Jenny                                                                            |      420   |                ✓ |            ✓ |
| Lohenstein__Großmütiger_Feldherr_Arminius_Buch_1_bis_5                                   |    1,130   |                ✓ |            ✓ |
| Mann__Der_Zauberberg                                                                     |      480   |                ✓ |            ✓ |
| May__Winnetou_II                                                                         |      500   |                ✓ |            ✓ |
| Musil__Der_Mann_ohne_Eigenschaften                                                       |      420   |                ✓ |            ✓ |
| Nicolai__Das_Leben_und_die_Meinungen_des_Herrn_Magister_Sebaldus_Nothanker               |      600   |                ✓ |            ✓ |
| Novalis__Die_Lehrlinge_zu_Sais                                                           |      440   |                ✓ |            ✓ |
| Reuter__Schelmuffskys_kuriose_und_sehr_gefährliche_Reisebeschreibung_zu_Wasser_und_Lande |      460   |                ✓ |            ✓ |
| Rilke__Die_Aufzeichnungen_des_Malte_Laurids_Brigge                                       |      430   |                ✓ |            ✓ |
| Schiller__Der_Geisterseher                                                               |      461   |                ✓ |            ✓ |
| Schnabel__Die_Insel_Felsenburg                                                           |      520   |                ✓ |            ✓ |
| Seghers__Das_siebte_Kreuz                                                                |      440   |                ✓ |            ✓ |
| Stockfleth__Die_Kunst_und_Tugend_gezierte_Macarie                                        |      800   |                ✓ |            ✓ |
| Storm__Der_Schimmelreiter                                                                |      455   |                ✓ |            ✓ |
| Wernicke__Ein_Heldengedicht_Hans_Sachs_genannt                                           |      385   |                ✓ |            ✓ |
| Wieland__Geschichte_des_Agathon                                                          |      500   |                ✓ |            ✓ |
| Wildermuth__Die_drei_Zöpfe                                                               |      170   |                ✓ |            ✓ |
| Zesen__Adriatische_Rosemund                                                              |      620   |                ✓ |            ✓ |
| **34 texts**                                                                             | **16,893** |           **34** |       **34** |

### v5.0 (2023-04-01)

| Text                                                                                     |  Sentences | GI, Comment, NfR | Attr, Change |
| :--------------------------------------------------------------------------------------- | ---------: | :--------------: | :----------: |
| Andreae__Die_chymische_Hochzeit                                                          |      462   |                ✓ |            ✓ |
| Bürger__Münchhausen                                                                      |      620   |                ✓ |            ✓ |
| Dahn__Kampf_um_Rom_ab_Kapitel_2                                                          |      420   |                ✓ |            ✓ |
| Fontane__Der_Stechlin                                                                    |      580   |                ✓ |            ✓ |
| Gellert__Das_Leben_der_schwedischen_Gräfin_von_G                                         |      420   |                ✓ |            ✓ |
| Goethe__Die_Wahlverwandtschaften                                                         |      687   |                ✓ |            ✓ |
| Grillparzer__Der_arme_Spielmann                                                          |      420   |                ✓ |            ✓ |
| Grimmelshausen__Der_abenteuerliche_Simplicissimus                                        |      499   |                ✓ |            ✓ |
| Hoffmann__Der_Sandmann                                                                   |      411   |                ✓ |            ✓ |
| Hölderlin__Hyperion_oder_der_Eremit_in_Griechenland                                      |      440   |                ✓ |            ✓ |
| Kafka__Der_Bau                                                                           |      420   |                ✓ |            ✓ |
| Keller__Der_grüne_Heinrich                                                               |      490   |                ✓ |            ✓ |
| Kleist__Michael_Kohlhaas                                                                 |      443   |                ✓ |            ✓ |
| Knigge__Die_Reise_nach_Braunschweig                                                      |      460   |                ✓ |            ✓ |
| Kotzebue__Das_merkwürdigste_Jahr_meines_Lebens                                           |      470   |                ✓ |            ✓ |
| LaRoche__Geschichte_des_Fräuleins_von_Sternheim                                          |      420   |                ✓ |            ✓ |
| Lewald__Jenny                                                                            |      420   |                ✓ |            ✓ |
| Lohenstein__Großmütiger_Feldherr_Arminius_Buch_1_bis_5                                   |    1,130   |                ✓ |            ✓ |
| Mann__Der_Zauberberg                                                                     |      480   |                ✓ |            ✓ |
| May__Winnetou_II                                                                         |      500   |                ✓ |            ✓ |
| Musil__Der_Mann_ohne_Eigenschaften                                                       |      420   |                ✓ |            ✓ |
| Nicolai__Das_Leben_und_die_Meinungen_des_Herrn_Magister_Sebaldus_Nothanker               |      600   |                ✓ |            ✓ |
| Novalis__Die_Lehrlinge_zu_Sais                                                           |      440   |                ✓ |            ✓ |
| Reuter__Schelmuffskys_kuriose_und_sehr_gefährliche_Reisebeschreibung_zu_Wasser_und_Lande |      460   |                ✓ |            ✓ |
| Rilke__Die_Aufzeichnungen_des_Malte_Laurids_Brigge                                       |      430   |                ✓ |            ✓ |
| Schiller__Der_Geisterseher                                                               |      461   |                ✓ |            ✓ |
| Schnabel__Die_Insel_Felsenburg                                                           |      520   |                ✓ |            ✓ |
| Seghers__Das_siebte_Kreuz                                                                |      440   |                ✓ |            ✓ |
| Stockfleth__Die_Kunst_und_Tugend_gezierte_Macarie                                        |      800   |                ✓ |            ✓ |
| Storm__Der_Schimmelreiter                                                                |      455   |                ✓ |            ✓ |
| Wernicke__Ein_Heldengedicht_Hans_Sachs_genannt                                           |      348   |                ✓ |            ✓ |
| Wieland__Geschichte_des_Agathon                                                          |      500   |                ✓ |            ✓ |
| Wildermuth__Die_drei_Zöpfe                                                               |      170   |                ✓ |            ✓ |
| Zesen__Adriatische_Rosemund                                                              |      620   |                ✓ |            ✓ |
| **34 texts**                                                                             | **16,856** |           **34** |       **34** |

### v4.1 (2022-09-12)

| Text                                                                                     | Sentences | GI, Comment, NfR | Attr, Change |
| :--------------------------------------------------------------------------------------- | --------: | :--------------: | :----------: |
| Andreae__Die_chymische_Hochzeit                                                          |     218   |                ✓ |            ✓ |
| Dahn__Kampf_um_Rom_ab_Kapitel_2                                                          |     206   |                ✓ |            ✓ |
| Fontane__Der_Stechlin                                                                    |     580   |                ✓ |            ✓ |
| Gellert__Das_Leben_der_schwedischen_Gräfin_von_G                                         |     210   |                ✓ |            ✓ |
| Goethe__Die_Wahlverwandtschaften                                                         |     687   |                ✓ |            ✓ |
| Grillparzer__Der_arme_Spielmann                                                          |     420   |                ✓ |            ✓ |
| Grimmelshausen__Der_abenteuerliche_Simplicissimus                                        |     221   |                ✓ |            ✓ |
| Hoffmann__Der_Sandmann                                                                   |     203   |                ✓ |            ✓ |
| Hölderlin__Hyperion_oder_der_Eremit_in_Griechenland                                      |     440   |                ✓ |            ✓ |
| Kafka__Der_Bau                                                                           |     207   |                ✓ |            ✓ |
| Kleist__Michael_Kohlhaas                                                                 |     443   |                ✓ |            ✓ |
| LaRoche__Geschichte_des_Fräuleins_von_Sternheim                                          |     217   |                ✓ |            ✓ |
| Mann__Der_Zauberberg                                                                     |     480   |                ✓ |            ✓ |
| May__Winnetou_II                                                                         |     500   |                ✓ |            ✓ |
| Musil__Der_Mann_ohne_Eigenschaften                                                       |     420   |                ✓ |            ✓ |
| Novalis__Die_Lehrlinge_zu_Sais                                                           |     440   |                ✓ |            ✓ |
| Reuter__Schelmuffskys_kuriose_und_sehr_gefährliche_Reisebeschreibung_zu_Wasser_und_Lande |     460   |                ✓ |            ✓ |
| Rilke__Die_Aufzeichnungen_des_Malte_Laurids_Brigge                                       |     430   |                ✓ |            ✓ |
| Schnabel__Die_Insel_Felsenburg                                                           |     300   |                ✓ |            ✓ |
| Seghers__Das_siebte_Kreuz                                                                |     440   |                ✓ |            ✓ |
| Wieland__Geschichte_des_Agathon                                                          |     500   |                ✓ |            ✓ |
| Zesen__Adriatische_Rosemund                                                              |     300   |                ✓ |            ✓ |
| **22 texts**                                                                             | **8,322** |           **22** |       **22** |

### v4.0 (2022-05-03)

| Text                                                                                     | Sentences | GI, Comment, NfR | Attr, Change |
| :--------------------------------------------------------------------------------------- | --------: | :--------------: | :----------: |
| Andreae__Die_chymische_Hochzeit                                                          |     218   |                ✓ |            ✓ |
| Dahn__Kampf_um_Rom_ab_Kapitel_2                                                          |     206   |                ✓ |            ✓ |
| Fontane__Der_Stechlin                                                                    |     375   |                ✓ |            ✓ |
| Gellert__Das_Leben_der_schwedischen_Gräfin_von_G                                         |     210   |                ✓ |            ✓ |
| Goethe__Die_Wahlverwandtschaften                                                         |     687   |                ✓ |            ✓ |
| Grillparzer__Der_arme_Spielmann                                                          |     420   |                ✓ |            ✗ |
| Grimmelshausen__Der_abenteuerliche_Simplicissimus                                        |     221   |                ✓ |            ✓ |
| Hoffmann__Der_Sandmann                                                                   |     203   |                ✓ |            ✓ |
| Hölderlin__Hyperion_oder_der_Eremit_in_Griechenland                                      |     440   |                ✓ |            ✓ |
| Kafka__Der_Bau                                                                           |     207   |                ✓ |            ✓ |
| Kleist__Michael_Kohlhaas                                                                 |     241   |                ✓ |            ✓ |
| LaRoche__Geschichte_des_Fräuleins_von_Sternheim                                          |     217   |                ✓ |            ✓ |
| Mann__Der_Zauberberg                                                                     |     235   |                ✓ |            ✓ |
| May__Winnetou_II                                                                         |     250   |                ✓ |            ✓ |
| Musil__Der_Mann_ohne_Eigenschaften                                                       |     207   |                ✓ |            ✓ |
| Novalis__Die_Lehrlinge_zu_Sais                                                           |     230   |                ✓ |            ✓ |
| Reuter__Schelmuffskys_kuriose_und_sehr_gefährliche_Reisebeschreibung_zu_Wasser_und_Lande |     460   |                ✓ |            ✗ |
| Rilke__Die_Aufzeichnungen_des_Malte_Laurids_Brigge                                       |     430   |                ✓ |            ✓ |
| Schnabel__Die_Insel_Felsenburg                                                           |     300   |                ✓ |            ✓ |
| Seghers__Das_siebte_Kreuz                                                                |     220   |                ✓ |            ✓ |
| Wieland__Geschichte_des_Agathon                                                          |     278   |                ✓ |            ✓ |
| Zesen__Adriatische_Rosemund                                                              |     300   |                ✓ |            ✓ |
| **22 texts**                                                                             | **6,555** |           **22** |       **20** |

### v3.1 (2022-03-31)

| Text                                                | Sentences | GI, Comment, NfR | Attr, Change |
| :-------------------------------------------------- | --------: | :--------------: | :----------: |
| Andreae__Die_chymische_Hochzeit                     |     218   |                ✓ |            ✓ |
| Dahn__Kampf_um_Rom_ab_Kapitel_2                     |     206   |                ✓ |            ✓ |
| Fontane__Der_Stechlin                               |     375   |                ✓ |            ✓ |
| Gellert__Das_Leben_der_schwedischen_Gräfin_von_G    |     210   |                ✓ |            ✓ |
| Goethe__Die_Wahlverwandtschaften                    |     687   |                ✓ |            ✓ |
| Grimmelshausen__Der_abenteuerliche_Simplicissimus   |     221   |                ✓ |            ✓ |
| Hoffmann__Der_Sandmann                              |     203   |                ✓ |            ✓ |
| Hölderlin__Hyperion_oder_der_Eremit_in_Griechenland |     228   |                ✓ |            ✓ |
| Kafka__Der_Bau                                      |     207   |                ✓ |            ✓ |
| Kleist__Michael_Kohlhaas                            |     241   |                ✓ |            ✓ |
| LaRoche__Geschichte_des_Fräuleins_von_Sternheim     |     217   |                ✓ |            ✓ |
| Mann__Der_Zauberberg                                |     235   |                ✓ |            ✓ |
| May__Winnetou_II                                    |     250   |                ✓ |            ✓ |
| Musil__Der_Mann_ohne_Eigenschaften                  |     207   |                ✓ |            ✓ |
| Novalis__Die_Lehrlinge_zu_Sais                      |     230   |                ✓ |            ✓ |
| Rilke__Die_Aufzeichnungen_des_Malte_Laurids_Brigge  |     430   |                ✓ |            ✗ |
| Schnabel__Die_Insel_Felsenburg                      |     300   |                ✓ |            ✓ |
| Seghers__Das_siebte_Kreuz                           |     220   |                ✓ |            ✓ |
| Wieland__Geschichte_des_Agathon                     |     278   |                ✓ |            ✓ |
| Zesen__Adriatische_Rosemund                         |     300   |                ✓ |            ✓ |
| **20 texts**                                        | **5,463** |           **20** |       **19** |

### v3.0 (2022-03-26)

| Text                                                | Sentences | GI, Comment, NfR | Attr, Change |
| :-------------------------------------------------- | --------: | :--------------: | :----------: |
| Andreae__Die_chymische_Hochzeit                     |     218   |                ✓ |            ✓ |
| Dahn__Kampf_um_Rom_ab_Kapitel_2                     |     206   |                ✓ |            ✓ |
| Fontane__Der_Stechlin                               |     375   |                ✓ |            ✓ |
| Gellert__Das_Leben_der_schwedischen_Gräfin_von_G    |     210   |                ✓ |            ✓ |
| Goethe__Die_Wahlverwandtschaften                    |     687   |                ✓ |            ✓ |
| Grimmelshausen__Der_abenteuerliche_Simplicissimus   |     221   |                ✓ |            ✓ |
| Hoffmann__Der_Sandmann                              |     203   |                ✓ |            ✓ |
| Hölderlin__Hyperion_oder_der_Eremit_in_Griechenland |     228   |                ✓ |            ✓ |
| Kafka__Der_Bau                                      |     207   |                ✓ |            ✓ |
| Kleist__Michael_Kohlhaas                            |     241   |                ✓ |            ✓ |
| LaRoche__Geschichte_des_Fräuleins_von_Sternheim     |     217   |                ✓ |            ✓ |
| Mann__Der_Zauberberg                                |     235   |                ✓ |            ✓ |
| May__Winnetou_II                                    |     250   |                ✓ |            ✓ |
| Musil__Der_Mann_ohne_Eigenschaften                  |     207   |                ✓ |            ✓ |
| Novalis__Die_Lehrlinge_zu_Sais                      |     230   |                ✓ |            ✓ |
| Rilke__Die_Aufzeichnungen_des_Malte_Laurids_Brigge  |     204   |                ✓ |            ✗ |
| Schnabel__Die_Insel_Felsenburg                      |     300   |                ✓ |            ✓ |
| Seghers__Das_siebte_Kreuz                           |     220   |                ✓ |            ✓ |
| Wieland__Geschichte_des_Agathon                     |     278   |                ✓ |            ✓ |
| Zesen__Adriatische_Rosemund                         |     300   |                ✓ |            ✓ |
| **20 texts**                                        | **5,237** |           **20** |       **19** |

### v2.0 (2021-12-09)

| Text                                                | Sentences | GI, Comment, NfR | Attr1, Attr2, Attr3, Attr4 |
| :-------------------------------------------------- | --------: | :--------------: | :------------------------: |
| Andreae__Die_chymische_Hochzeit                     |     218   |                ✓ |                          ✓ |
| Dahn__Kampf_um_Rom_ab_Kapitel_2                     |     206   |                ✓ |                          ✓ |
| Fontane__Der_Stechlin                               |     375   |                ✓ |                          ✓ |
| Gellert__Das_Leben_der_schwedischen_Gräfin_von_G    |     210   |                ✓ |                          ✓ |
| Goethe__Die_Wahlverwandtschaften                    |     687   |                ✓ |                          ✓ |
| Grimmelshausen__Der_abenteuerliche_Simplicissimus   |     221   |                ✓ |                          ✓ |
| Hoffmann__Der_Sandmann                              |     203   |                ✓ |                          ✓ |
| Hölderlin__Hyperion_oder_der_Eremit_in_Griechenland |     228   |                ✓ |                          ✗ |
| Kafka__Der_Bau                                      |     207   |                ✓ |                          ✓ |
| Kleist__Michael_Kohlhaas                            |     241   |                ✓ |                          ✗ |
| LaRoche__Geschichte_des_Fräuleins_von_Sternheim     |     217   |                ✓ |                          ✓ |
| Mann__Der_Zauberberg                                |     235   |                ✓ |                          ✗ |
| May__Winnetou_II                                    |     250   |                ✓ |                          ✗ |
| Musil__Der_Mann_ohne_Eigenschaften                  |     207   |                ✓ |                          ✓ |
| Novalis__Die_Lehrlinge_zu_Sais                      |     230   |                ✓ |                          ✓ |
| Schnabel__Die_Insel_Felsenburg                      |     300   |                ✓ |                          ✓ |
| Seghers__Das_siebte_Kreuz                           |     220   |                ✓ |                          ✗ |
| Wieland__Geschichte_des_Agathon                     |     278   |                ✓ |                          ✓ |
| Zesen__Adriatische_Rosemund                         |     300   |                ✓ |                          ✓ |
| **19 texts**                                        | **5,033** |           **19** |                     **14** |

### v1.1 (2021-11-16)

| Text                                             | Sentences | GI, Comment, NfR | Attr1, Attr2, Attr3 |
| :----------------------------------------------- | --------: | :--------------: | :-----------------: |
| Dahn__Kampf_um_Rom_ab_Kapitel_2                  |     200   |                ✓ |                   ✓ |
| Fontane__Der_Stechlin                            |     375   |                ✓ |                   ✓ |
| Gellert__Das_Leben_der_schwedischen_Gräfin_von_G |     200   |                ✓ |                   ✓ |
| Goethe__Die_Wahlverwandtschaften                 |     686   |                ✓ |                   ✓ |
| Hoffmann__Der_Sandmann                           |     205   |                ✓ |                   ✓ |
| Kafka__Der_Bau                                   |     207   |                ✓ |                   ✓ |
| LaRoche__Geschichte_des_Fräuleins_von_Sternheim  |     205   |                ✓ |                   ✓ |
| Musil__Der_Mann_ohne_Eigenschaften               |     205   |                ✓ |                   ✓ |
| Novalis__Die_Lehrlinge_zu_Sais                   |     231   |                ✓ |                   ✓ |
| Wieland__Geschichte_des_Agathon                  |     276   |                ✓ |                   ✓ |
| **10 texts**                                     | **2,790** |           **10** |              **10** |